import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import cv2
import random
import  pickle
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import pickle
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Activation,Dropout,Flatten,Conv2D,MaxPooling2D,BatchNormalization


DATADIR = "C:/Users/Kresimir Markota/Desktop/Projekt Pokedex/izdvojeniDataset/"
CATEGORIES = ["Charmander", "Pikachu","Mewtwo","Bulbasaur","Squirtle"]

for category in CATEGORIES:
    path = os.path.join(DATADIR,category)
    for img in os.listdir(path):
        img_array = cv2.imread(os.path.join(path,img))

        break
    break
# resize image
IMG_SIZE=96
new_array= cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))


training_data = []
# image resize
def create_training_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img))
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                training_data.append([new_array,class_num])
            except Exception as e:
                pass

create_training_data()

# shuffling training data
random.shuffle(training_data)

X2 = []
y2 = []

# appending features and labels to list
for features, labels in training_data:
    X2.append(features)
    y2.append(labels)


X2= np.array(X2).reshape(-1,IMG_SIZE,IMG_SIZE, 3)

# Data is saved in pickle file and they are now ready for training
pickle_out = open("X2.pickle","wb")
pickle.dump(X2, pickle_out)
pickle_out.close()

pickle_out = open("y2.pickle","wb")
pickle.dump(y2, pickle_out)
pickle_out.close()




import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import pickle
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Activation,Dropout,Flatten,Conv2D,MaxPooling2D,BatchNormalization
from tensorflow.keras import backend as K
import time




gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


aug = ImageDataGenerator(rotation_range=25, width_shift_range=0.1,
	height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
	horizontal_flip=True, fill_mode="nearest")

pickle_in = open("X2.pickle","rb")
X= pickle.load(pickle_in)

pickle_in = open("y2.pickle","rb")
y= pickle.load(pickle_in)

X=X/255.0
(trainX, testX, trainY, testY) = train_test_split(X,y, test_size=0.2, random_state=42)



model =Sequential()

input_shape=(96,96,3)
chanDim = -1

if K.image_data_format() == "channels_first":
    input_shape = (96,96,3)
    chanDim = 1



model.add(Conv2D(32, (3, 3), padding="same",
                             input_shape=input_shape))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(MaxPooling2D(pool_size=(3, 3)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(Conv2D(64, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))


model.add(Conv2D(128, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(Conv2D(128, (3, 3), padding="same"))
model.add(Activation("relu"))
model.add(BatchNormalization(axis=chanDim))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(1024))
model.add(Activation("relu"))
model.add(BatchNormalization())
model.add(Dropout(0.5))

# softmax classifier
model.add(Dense(5))
model.add(Activation("softmax"))
model.compile(loss="sparse_categorical_crossentropy",
              optimizer='adam',
              metrics=['accuracy'])

H = model.fit_generator(
	aug.flow(trainX, trainY, batch_size=32),
	validation_data=(testX, testY),
	steps_per_epoch=32 ,
	epochs=50, verbose=1)

# H=model.fit(aug.flow(trainX,trainY),batch_size=32,epochs=40, validation_split=0.3)

print("[INFO] serializing network...")
model.save("pokemon_model_sparse_15")

# save the label binarizer to disk
print("[INFO] serializing label binarizer...")
f = open(args["labelbin"], "wb")
f.write(pickle.dumps(lb))
f.close()

plt.style.use("ggplot")
plt.figure()
N = 50
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["acc"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_acc"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")
plt.savefig("plot7")


import cv2
import tensorflow as tf
from tensorflow import keras
import pickle
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
from tensorflow import confusion_matrix
import numpy as np
from sklearn.metrics import classification_report,confusion_matrix

CATEGORIES = ["Charmander","Pikachu","Mewtwo","Bulbasaur","Squirtle"]
model = keras.models.load_model("pokemon_model_2")

pickle_in = open("X.pickle","rb")
X= pickle.load(pickle_in)

pickle_in = open("y.pickle","rb")
y= pickle.load(pickle_in)

X=X/255.0

(trainX, testX, trainY, testY) = train_test_split(X,y, test_size=0.2, random_state=42)

def prepare(filepath):
    IMG_SIZE = 96
    img_array = cv2.imread(filepath)
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 3)





y_pred=model.predict_classes(testX)
print("Matrica zabune")

con_mat = confusion_matrix(testY,y_pred)
print(con_mat)
print("-------------------------------------------")
print("Clasification report")

target_names = ["Charmander","Pikachu","Mewtwo","Bulbasaur","Squirtle"]

print(classification_report(testY,y_pred,target_names=target_names))









